import psycopg2
import hidden
import pandas as pd
import sdv

from sdv.metrics.relational import KSTestExtended
# from sdv.metrics.relational import LogisticDetection
# from sdv.metrics.relational import SVCDetection
# from sdv.metrics.relational import BNLogLikelihood
from sdv.metrics.relational import LogisticParentChildDetection
# from sdv.metrics.relational import SVCParentChildDetection

# from sdv.evaluation import evaluate

# Load the secrets
secrets = hidden.secrets()

param_dic = {
    'host': secrets['host'],
    'port': secrets['port'],
    'database': secrets['database'],
    'user': secrets['user'],
    'password': secrets['pass']
}


def connect(params_dic):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    print('Connection successful')
    return conn


def postgresql_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute('SET search_path TO cdm_531')
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print('Error: %s' % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


# Connect to the database

conn = connect(param_dic)

# Tables column names

column_names_person = ['person_id', 'gender_concept_id', 'year_of_birth', 
                       'month_of_birth', 'day_of_birth','birth_date_time', 
                       'race_concept_id', 'ethnicity_concept_id', 
                       'location_id', 'provider_id', 'care_site_id', 
                       'person_source_value', 'gender_source_value', 
                       'gender_source_concept_id', 'race_source_value', 
                       'race_source_concept_id', 'ethnicity_source_value', 
                       'ethnicity_source_concept_id']

column_names_condition_occurrence = ['condition_occurrence_id', 'person_id', 
                                     'condition_concept_id', 'condition_start_date',
                                     'condition_start_datetime','condition_end_date',
                                     'condition_end_datetime', 'condition_type_concept_id',
                                     'stop_reason', 'provider_id', 'visit_occurrence_id', 
                                     'condition_source_value', 'condition_source_concept_id', 
                                     'condition_status_source_value', 'condition_status_concept_id']

column_names_drug_exposure = ['drug_exposure_id', 'person_id', 'drug_concept_id', 
                             'drug_exposure_start_date', 'drug_exposure_start_datetime',
                             'drug_exposure_end_date', 'drug_exposure_end_datetime',
                             'verbatim_end_date', 'drug_type_concept_id', 'stop_reason',
                             'refills', 'quantity', 'days_supply', 'sig', 'route_concept_id',
                             'lot_number', 'provider_id', 'visit_occurrence_id', 
                             'drug_source_value', 'drug_source_concept_id', 
                             'route_source_value', 'dose_unit_source_value']

# Execute the "SELECT *" query to populate pandas dataframes

df_person = postgresql_to_dataframe(conn, "SELECT * FROM person", column_names_person)

df_condition_occurrence = postgresql_to_dataframe(conn, "SELECT * FROM condition_occurrence", column_names_condition_occurrence)

df_drug_exposure = postgresql_to_dataframe(conn, "SELECT * FROM drug_exposure", column_names_drug_exposure)

# Close the connection
conn.close()

# Specify tables dictionnary for SDV

real_tables = {'person' : df_person, 'condition_occurrence' : df_condition_occurrence, 
          'drug_exposure' : df_drug_exposure}

# Specify the metadata for SDV

metadata = sdv.Metadata()

metadata.add_table(name = 'person', data = real_tables['person'], primary_key = 'person_id')

metadata.add_table(name = 'condition_occurrence', data = real_tables['condition_occurrence'], 
                   primary_key = 'condition_occurrence_id', parent = 'person',
                   foreign_key = 'person_id')

metadata.add_table(name = 'drug_exposure', data = real_tables['drug_exposure'], 
                   primary_key = 'drug_exposure_id', parent = 'person',
                   foreign_key = 'person_id')

# Visualize the metadata

# metadata.visualize() not working well

# Metadata dictionary check if types are correct

print(metadata)
print('\n')

print(metadata.to_dict())
print('\n')

# Model

model = sdv.relational.HMA1(metadata)

model.fit(real_tables)

model.save('model_1.pkl')

# Generate data

loaded = sdv.relational.HMA1.load('model_1.pkl')

synthetic_tables = loaded.sample()

# print('Evaluate \n')

# print('Results from evaluate function person table: ', 
#       evaluate(synthetic_tables['person'], real_tables['person']))
# print('\n')
# print('Results from evaluate function condition occurrence table: ', 
#       evaluate(synthetic_tables['condition_occurrence'], real_tables['condition_occurrence']))
# print('\n')
# print('Results from evaluate function drug exposure table: ', 
#       evaluate(synthetic_tables['drug_exposure'], real_tables['drug_exposure']))

print('\n')

print('Multi single table metrics \n')

print('KS test extended: ', KSTestExtended.compute(real_tables, synthetic_tables))
# print('Logistic detection: ', LogisticDetection.compute(real_tables, synthetic_tables))
# print('SVC detection: ', SVCDetection.compute(real_tables, synthetic_tables))
# print('BN Log-Likelihood detection: ', BNLogLikelihood.compute(real_tables, synthetic_tables))

print('Parent-child detection metrics \n')

print('Logistic parent-child detection: ', LogisticParentChildDetection.compute(real_tables, synthetic_tables, metadata))
# print('SVC parent-child detection: ', SVCParentChildDetection.compute(real_tables, synthetic_tables, metadata))